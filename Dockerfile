FROM ubuntu:jammy

RUN apt update && apt upgrade -y && \
    apt install -y build-essential git p7zip-full && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists

COPY entry.sh /usr/local/bin/entry.sh

RUN set -ex && \
    chmod +x /usr/local/bin/entry.sh

CMD ["entry.sh"]
